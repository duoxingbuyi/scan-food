import request from './request';

/**
 * 设置全局配置
 */
request.prototype.setConfig({
	// 基地址
	url: 'https://mini.czttn.top/diancang/',
	contentType: 'json',
});

/**
 * 全局拦截器
 */
request.prototype.addGlobalInterce({
	/**
	 * 请求拦截器 (例如配置token)
	 * return false或者不return值, 都不会发送请求
	 * @param {Object} config
	 */
	request(config) {
		return config;
	},

	/**
	 * 响应拦截器 (例如根据状态码拦截数据)
	 * return false或者不return值 则都不会返回值
	 * return Promise.reject('xxxxx')，主动抛出错误
	 * @param {Object} res 
	 */
	response(res) {
		let firstCodeNum = String(res.statusCode).substr(0, 1);
		// 2xx
		if (firstCodeNum === '2') {
			return res;
		}

		// 3xx
		if (firstCodeNum === '3') {
			uni.showToast({
				title: '请求失败',
				duration: 1500
			})
			return res;
		}

		// 4xx or 5xx
		if (firstCodeNum === '4' || firstCodeNum === '5') {
			uni.showToast({
				title: '服务器繁忙',
				duration: 1500
			})
			return Promise.reject('nooooo')
		}

		// 停止发送请求 request.stop()
		if (JSON.stringify(res) === '{"errMsg":"request:fail abort"}') {
			return false;
		}
		return res;
	}
});

export default request
