import request from '../common/request/index.js'

export default {
    /**
	 * 查询我的订单
	 */
    getList ({ success, fail, complete } = {}, canRunState = true) {
        const instance = new request();
        const r = instance.get({
            url: "app/order/list",
            contentType: 'json',
            success: res => {
                success && success(res)
            },
            fail: err => {
                fail && fail(err)
            },
            complete: res => {
                complete && complete(res)
            }
        });
        // 停止发生请求
        !canRunState && instance.stop(r)
    }

}