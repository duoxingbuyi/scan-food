import request from '../common/request/index.js'

export default {
    /**
	 * 查询商家列表
	 */
    getList ({ success, fail, complete } = {}, canRunState = true) {
        const instance = new request();
        const r = instance.get({
            url: "app/businessman/list",
            contentType: 'json',
            success: res => {
                success && success(res)
            },
            fail: err => {
                fail && fail(err)
            },
            complete: res => {
                complete && complete(res)
            }
        });
        // 停止发生请求
        !canRunState && instance.stop(r)
    },
	
	/**
	 * 查询附近商家
	 */
	getnearby ({ success, fail, complete } = {}, canRunState = true,formdata={}) {
	    const instance = new request();
	    const r = instance.get({
	        url: "app/businessman/nearby",
	        contentType: 'json',
			data: formdata,
	        success: res => {
	            success && success(res)
	        },
	        fail: err => {
	            fail && fail(err)
	        },
	        complete: res => {
	            complete && complete(res)
	        }
	    });
	    // 停止发生请求
	    !canRunState && instance.stop(r)
	}

}