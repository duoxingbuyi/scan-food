import request from '../common/request/index.js'

export default {
	/**
	 * 微信登录
	 */
    wxlogin ({ success, fail, complete } = {}, canRunState = true,formdata={}) {
        const instance = new request();
        const r = instance.post({
            url: "app/wxlogin",
            contentType: 'form',
			data: formdata,
            success: res => {
                success && success(res)
            },
            fail: err => {
                fail && fail(err)
            },
            complete: res => {
                complete && complete(res)
            }
        });
        // 停止发生请求
        !canRunState && instance.stop(r)
    }

}