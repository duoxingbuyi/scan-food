import Vue from 'vue'
import Vuex from 'vuex'
import { mapState } from 'vuex'

Vue.use(Vuex)


const store = new Vuex.Store({
	state: {
		hasLogin: uni.getStorageSync('token')?true:false,
		userInfo: uni.getStorageSync('userInfo')?uni.getStorageSync('userInfo'):{},
		token: uni.getStorageSync('token')?uni.getStorageSync('token'):'',
		businessmanInfo: {}
	},
	mutations: {
		/**
		 * 登录
		 * @param {Object} state
		 * @param {Object} provider
		 */
		login(state, provider) {
			state.hasLogin = true;
			state.token = provider.token;
			state.userInfo = provider.userInfo;
			//缓存用户登陆状态
			uni.setStorage({
				key: 'token',
				data: provider.token
			})
			//缓存用户信息
			uni.setStorage({
				key: 'userInfo',
				data: provider.userInfo
			})
		},
		/**
		 * 退出登录
		 * @param {Object} state
		 */
		logout(state) {
			state.hasLogin = false;
			state.userInfo = {};
			state.token = ''
			uni.removeStorage({
				key: 'userInfo'
			})
			uni.removeStorage({
				key: 'token'
			})
		},
		/**
		 * 缓存所选商家
		 * @param {Object} state
		 * @param {Object} provider
		 */
		selectBusiness(state, provider) {
			state.businessmanInfo = provider;
			uni.setStorageSync('businessmanInfo', provider)
		},
	},
	actions: {

	}
})

export default store
